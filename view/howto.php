<?php
	include 'core/config.php';
?>
<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>How To Vote</title>

		<link href="<?= ASSET ?>semantic/semantic.min.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style>
		.main, .container{
			height: 100%;
		}
		</style>
	</head>
	<body>
		<div class="container" id="content">
			<div class="main ui inverted vertical segment" style="padding-top:5%;">
		    	<div class="ui page two column grid">
		    		<div id="step" class="column">
		    			<div class="ui small sticky vertical ordered steps">
							<div id="step1" class="ui active step">
								<div class="content">
									<div class="title">Shipping</div>
									<div class="description">Choose your shipping options</div>
								</div>
							</div>
							<div id="step2" class="ui step">
								<div class="content">
									<div class="title">Shipping</div>
									<div class="description">Choose your shipping options</div>
								</div>
							</div>
							<div id="step3" class="ui step">
								<div class="content">
									<div class="title">Shipping</div>
									<div class="description">Choose your shipping options</div>
								</div>
							</div>
							<div id="step4" class="ui step">
								<div class="content">
									<div class="title">Shipping</div>
									<div class="description">Choose your shipping options</div>
								</div>
							</div>
							<div id="step5" class="ui step">
								<div class="content">
									<div class="title">Shipping</div>
									<div class="description">Choose your shipping options</div>
								</div>
							</div>
							<div id="step6" class="ui step">
								<div class="content">
									<div class="title">Shipping</div>
									<div class="description">Choose your shipping options</div>
								</div>
							</div>
							<div id="step7" class="ui step">
								<div class="content">
									<div class="title">Shipping</div>
									<div class="description">Choose your shipping options</div>
								</div>
							</div>
						</div>
		    		</div>
		      		<div class="column">
		        		<h2 class="ui inverted header">Bagaimana Cara Voting Ketua HIMA IEC ?</h2>
		        		<div class="column">
						    <div id="howto1" class="ui raised segment">
						      <a class="ui blue ribbon label">Step 1</a>
						      <h3><span>Login Email ITHB</span></h3>
						      <p>buka link <a href="http://students.ithb.ac.id/">ITHB Student mail</a> lalu login menggunakan id anda.</p>
						      <img class="ui centered medium rounded image" src="<?= ASSET ?>images/howto/step1.png">
						    </div>
						    <div id="howto2" class="ui raised segment">
						      <a class="ui blue ribbon label">Step 2</a>
						      <h3><span>Login Email ITHB</span></h3>
						      <p>buka link <a href="http://students.ithb.ac.id/">ITHB Student mail</a> lalu login menggunakan id anda.</p>
						      <img class="ui centered medium rounded image" src="<?= ASSET ?>images/howto/step1.png">
						    </div>
						    <div id="howto3" class="ui raised segment">
						      <a class="ui blue ribbon label">Step 3</a>
						      <h3><span>Login Email ITHB</span></h3>
						      <p>buka link <a href="http://students.ithb.ac.id/">ITHB Student mail</a> lalu login menggunakan id anda.</p>
						      <img class="ui centered medium rounded image" src="<?= ASSET ?>images/howto/step1.png">
						    </div>
						    <div id="howto4" class="ui raised segment">
						      <a class="ui blue ribbon label">Step 4</a>
						      <h3><span>Login Email ITHB</span></h3>
						      <p>buka link <a href="http://students.ithb.ac.id/">ITHB Student mail</a> lalu login menggunakan id anda.</p>
						      <img class="ui centered medium rounded image" src="<?= ASSET ?>images/howto/step1.png">
						    </div>
						    <div id="howto5" class="ui raised segment">
						      <a class="ui blue ribbon label">Step 5</a>
						      <h3><span>Login Email ITHB</span></h3>
						      <p>buka link <a href="http://students.ithb.ac.id/">ITHB Student mail</a> lalu login menggunakan id anda.</p>
						      <img class="ui centered medium rounded image" src="<?= ASSET ?>images/howto/step1.png">
						    </div>
						    <div id="howto6" class="ui raised segment">
						      <a class="ui blue ribbon label">Step 6</a>
						      <h3><span>Login Email ITHB</span></h3>
						      <p>buka link <a href="http://students.ithb.ac.id/">ITHB Student mail</a> lalu login menggunakan id anda.</p>
						      <img class="ui centered medium rounded image" src="<?= ASSET ?>images/howto/step1.png">
						    </div>
						    <div id="howto7" class="ui raised segment">
						      <a class="ui blue ribbon label">Step 7</a>
						      <h3><span>Login Email ITHB</span></h3>
						      <p>buka link <a href="http://students.ithb.ac.id/">ITHB Student mail</a> lalu login menggunakan id anda.</p>
						      <img class="ui centered medium rounded image" src="<?= ASSET ?>images/howto/step1.png">
						    </div>
						    <div class="negative ui animated button">
								<div class="visible content">Previous Step</div>
							  	<div class="hidden content">
							    	<i class="left arrow icon"></i>
							  	</div>
							</div>
							<div class="positive ui animated button">
								<div class="visible content">Next Step</div>
							  	<div class="hidden content">
							    	<i class="right arrow icon"></i>
							  	</div>
							</div>
						</div>
		      		</div>
		    	</div>
		  	</div>
		</div>
		<!-- <div class="ui vertically padded page grid">
	    	<div class="column">
	      		<h1 class="ui black header">Column Background</h1>
	      		<p>Third section of content</p>
	    	</div>
	  	</div> -->
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<script src="<?= ASSET ?>semantic/semantic.min.js"></script>
		<script>
			$(document).ready(function() {
				var page = 0;
				for (var i = 2; i <= 7; i++) {
					$('#howto'+i).hide();
				};
				$('.negative.button').addClass('disabled');
				$('.negative.button').click(function(e){
					$('#step'+page).removeClass('completed',1000);
					$('#step'+(page+1)).removeClass('active',1000);
					$('#step'+page).addClass('active',1000);
					$('#howto'+(page+1)).transition('slide up','320ms');
					page = page - 1;
					window.setTimeout(function(){
						$('#howto'+(page+1)).transition('slide down','320ms');
					},350);
					if(page == 0){
						$('.negative.button').addClass('disabled');
					}
					if(page < 7){
						$('.positive.button').removeClass('disabled');
					}
				});

				$('.positive.button').click(function(e){
					page = page + 1;
					$('#howto'+page).transition('slide down','320ms');
					$('#step'+page).addClass('completed',1000);
					$('#step'+(page+1)).addClass('active',1000);
					$('#step'+page).removeClass('active',1000);
					if(page == 1){
						$('.negative.button').removeClass('disabled');
					}
					if(page == 7){
						$('.positive.button').addClass('disabled');
					}
					window.setTimeout(function(){
						$('#howto'+(page+1)).transition('slide up','320ms');
					},350);
				
				});

				if ($(window).width() > 320) {
				   $('#step').addClass('four wide');
				}
				else {
				   $('#step').removeClass('four wide');
				}
			});
		</script>
	</body>
</html>