<?php
	include 'controller/AuthController.php';
	if (!AuthController::user()) {
		header('location:home');
	}else{
?>
<!DOCTYPE html>
<html lang="id">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<h1 class="text-center">Vote</h1>
		<div class="container">
			<div class="row">
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-push-1">
					<div class="jumbotron">
						<div class="container">
							<h1>Kandidat 1</h1>
							<br/>
							<p>
								<a class="btn btn-primary btn-lg">Vote!</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-push-1">
					<div class="jumbotron">
						<div class="container">
							<h1>Kandidat 2</h1>
							<br/>
							<p>
								<a class="btn btn-primary btn-lg">Vote!</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</body>
</html>
<?php } ?>