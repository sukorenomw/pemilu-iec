<?php 
	require_once 'controller/GeneratorController.php';
	$generator = new GeneratorController();

	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
	 	$nim = isset($_POST['nim']) ? $_POST['nim'] : 0;
	 	echo $generator->generatePassword($nim);
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<?php if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) { ?>
		<div class="container">
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-push-2">
					<div class="page-header">
					  <h1>Passkey generator</h1>
					</div>
					<form id="data" action="" method="POST" role="form">
						<div class="form-group">
							<label for="">Input NIM : </label>
							<input type="text" class="form-control" id="nim" name="nim" placeholder="Masukan nim ...">
						</div>
					</form>
					<button id="callAjax" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Generate</button>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Passkey for nim : </h4>
                    </div>
                    <div class="modal-body">
                    	<div class="panel panel-primary">
                    		 <div class="panel-body" id="hasil-modal">
                    		 	
                    		 </div>
                    	</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> 
		<!-- jQuery -->
		
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script>
	        $(document).ready(function () {
	        	$('#callAjax').click(function (e) {
	        		$.ajax({
			            type: "POST",
			            url: "generator",
			            data: $('#data').serialize(),
			            success: function (responseText) {
							$('#hasil-modal').html(responseText);
							$('#nim').val("");
			            }
			        });
			        e.preventDefault();
			    });
	        });
        </script>
	</body>
</html>